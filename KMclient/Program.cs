﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace Client
{
    class Program
    {
        static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static void Main(string[] args)
        {
            socket.Connect("127.0.0.1", 1037);
            while (true)
            {
                string message = Console.ReadLine();
                byte[] buffer = Encoding.UTF8.GetBytes(message);
                socket.Send(buffer);
                byte[] buffer_1 = new byte[110];
                socket.Receive(buffer_1);
                string buff = Encoding.UTF8.GetString(buffer_1);
                using (FileStream fstream = new FileStream($"client.txt", FileMode.Append))
                {
                    // преобразуем строку в байты
                    DateTime date1 = DateTime.Now;
                    byte[] array = Encoding.UTF8.GetBytes(date1.ToString() + "\r\n" + buff + "\r\n");
                    // запись массива байтов в файл
                    fstream.Write(array, 0, array.Length);
                    //Console.WriteLine("Текст записан в файл");

                }
                Console.WriteLine(buff);

            }
            Console.ReadKey();
        }
    }
}