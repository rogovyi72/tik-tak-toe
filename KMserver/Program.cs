﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;


namespace Server
{
    class Program
    {
        static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static void Main(string[] args)
        {

            int[,] fight = {
                 {0,0,0,0,0},
                 {0,0,0,0,0},
                 {0,0,0,0,0},
                 {0,0,0,0,0},
                 {0,0,0,0,0},

            };
            socket.Bind(new IPEndPoint(IPAddress.Any, 1037));
            socket.Listen(5);
            Socket client = socket.Accept();
            Console.WriteLine("Client connected");
            byte[] buffer = new byte[10];
            bool quit = false;
            byte[] buffer_1 = new byte[110];

            bool end = true;
            while (!quit)
            {
                try
                {
                    client.Receive(buffer);
                    string buff = Encoding.UTF8.GetString(buffer);
                    bool cool = false;
                    if (string.Compare(buff, "who") == 0)
                        cool = true;
                    if (cool)
                    {
                        quit = false;
                        buffer_1 = Encoding.UTF8.GetBytes("Made by Semen Rohoviy\nGroup K-23\nVariant 12: tik-tak toe game\n");
                        client.Send(buffer_1);

                        break;
                    }
                    int i = (int)(buff[0]) - 49;
                    int j = (int)(buff[1]) - 49;
                    // Console.WriteLine(buff[2].ToString());
                    if (fight[i, j] == 0)
                    {
                        fight[i, j] = 1;
                        end = true;
                        for (int l = 0; l < 5; l++)
                        {
                            for (int h = 0; h < 5; h++)
                            {
                                if (fight[l, h] == 0) end = false;
                            }
                        }
                        if (end)
                        {
                            buffer_1 = Encoding.UTF8.GetBytes("Game over\r\nNew game\r\n");
                            client.Send(buffer_1);
                            for (int l = 0; l < 5; l++)
                            {
                                for (int h = 0; h < 5; h++)
                                {
                                    fight[l, h] = 0;
                                }
                            }
                        }
                        else
                        {
                            while (true)
                            {
                                Random rng = new Random();
                                i = rng.Next(5);
                                j = rng.Next(5);
                                if (fight[i, j] == 0)
                                {
                                    fight[i, j] = 2;
                                    break;
                                }
                            }
                        }
                    }
                    string s = "";
                    for (int l = 0; l < 5; l++)
                    {

                        for (int h = 0; h < 5; h++)
                        {
                            if (fight[l, h] == 0) s += ".";
                            if (fight[l, h] == 1) s += "X";
                            if (fight[l, h] == 2) s += "0";

                        }
                        s += "\r\n";

                    }
                    buffer_1 = Encoding.UTF8.GetBytes(s);
                    client.Send(buffer_1);

                    using (FileStream fstream = new FileStream($"server.txt", FileMode.Append))
                    {
                        // преобразуем строку в байты
                        DateTime date1 = DateTime.Now;
                        byte[] array = Encoding.UTF8.GetBytes(date1.ToString() + " " + buff + "\r\n");
                        // запись массива байтов в файл
                        fstream.Write(array, 0, array.Length);
                        Console.WriteLine("Data row added to logs");

                    }


                    for (int l = 0; l < 5; l++)
                    {
                        for (int h = 0; h < 5; h++)
                        {
                            if (fight[l, h] == 0) end = false;
                        }
                    }
                    if (end)
                    {
                        buffer_1 = Encoding.UTF8.GetBytes("Game over\nNew game\n");
                        client.Send(buffer_1);
                        for (int l = 0; l < 5; l++)
                        {
                            for (int h = 0; h < 5; h++)
                            {
                                fight[l, h] = 0;
                            }
                        }
                    }
                }
                catch
                {

                    Console.WriteLine("[Incorrect input! Server stoped!]");
                    quit = true;
                }
            }

            Console.ReadKey();
        }
    }
}